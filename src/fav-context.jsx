import { createContext, useCallback, useMemo, useState } from "react";

const FavContext = createContext({
  favs: [],
  isfav: (p) => {},
});

export const FavContextProvider = (props) => {
  const [userFavs, setuserFavs] = useState([]);

  const isfavHandler = useCallback(
    (filmId) => {
      return userFavs?.some((el) => el.id === filmId);
    },
    [userFavs]
  );

  const context = useMemo(
    () => ({
      favs: userFavs,
      isfav: isfavHandler,
      setuserFavs,
    }),
    [userFavs, isfavHandler]
  );

  return (
    <FavContext.Provider value={context}>{props.children}</FavContext.Provider>
  );
};

export default FavContext;
