import React, { createContext, useMemo, useReducer } from "react";
import { FILER_GENRE, FILER_LANG, FILER_STATUS } from "../registr/consts";
export const FilterFilmsContext = createContext();

export const filterReducer = (state, action) => {
  switch (action.type) {
    case FILER_GENRE:
      return {
        ...state,
        filterByGenre: action.payload.value,
      };
    case FILER_STATUS:
      return {
        ...state,
        filterByStatus: action.payload.value,
      };
    case FILER_LANG:
      return {
        ...state,
        filterByLang: action.payload.value,
      };
    default:
      return state;
  }
};

export const FilterFilmsContextProvider = (props) => {
  const [state, dispatch] = useReducer(filterReducer, {
    filterByGenre: "all",
    filterByLang: "all",
    filterByStatus: "all",
  });

  const value = useMemo(() => ({ state, dispatch }), [state]);

  return (
    <FilterFilmsContext.Provider value={value}>
      {props.children}
    </FilterFilmsContext.Provider>
  );
};
