import { createContext, useMemo, useState } from "react";

export const NetusersContext = createContext();

const NetusersContextProvider = (props) => {
  const [netusers, setnetusers] = useState([]);
  const value = useMemo(() => ({ netusers, setnetusers }), [netusers]);

  return (
    <NetusersContext.Provider value={value}>
      {props.children}
    </NetusersContext.Provider>
  );
};

export default NetusersContextProvider;
