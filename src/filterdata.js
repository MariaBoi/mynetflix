export const genres = [
  { id: 1, tag: "Comedy" },
  { id: 2, tag: "Action" },
  { tag: "Drama" },
  { tag: "Romance" },
  { tag: "Thriller" },
  { tag: "Horror" },
  { tag: "Medical" },
  { tag: "Crime" },
  { tag: "Supernatural" },
  { tag: "Anime" },
  { tag: "History" },

  { tag: "Espionage" },
  { tag: "Family" },
];

export const status = [{ tag: "Running" }, { tag: "Ended" }];

export const langs = [{ tag: "Japanese" }, { tag: "English" }];
