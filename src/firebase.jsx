import { getAuth, onAuthStateChanged } from "@firebase/auth";
import { initializeApp } from "firebase/app";
import { useState, useEffect, createContext, useMemo, useContext } from "react";

export const firebaseApp = initializeApp({
  apiKey: "AIzaSyAiKhEW-yLrEmSs6qI5vpsmS92csDa-oXc",
  authDomain: "nenflix.firebaseapp.com",
  projectId: "nenflix",
  storageBucket: "nenflix.appspot.com",
  messagingSenderId: "759361766442",
  appId: "1:759361766442:web:f89852fd8222d379a22f53",
  measurementId: "G-B2LX93WZDT",
});

export const AuthContext = createContext();

export const AuthContextProvider = (props) => {
  const [user, setUser] = useState();
  const [error, setError] = useState();

  useEffect(() => {
    onAuthStateChanged(getAuth(), setUser, setError);
  }, []);

  const value = useMemo(() => ({ user, error }), [user, error]);
  return (
    <AuthContext.Provider value={value}>{props.children}</AuthContext.Provider>
  );
};
