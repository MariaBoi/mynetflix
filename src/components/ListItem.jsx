import axios from "axios";
import { useCallback, useContext, useMemo, useState } from "react";
import { Link } from "react-router-dom";
import { NetusersContext } from "../context/NetusersContextProvider";
import FavContext from "../fav-context";
import { AuthContext } from "../firebase";
import { FavoriteButton } from "./FvoriteButton";
import classes from "./ListItem.module.css";

const ListItem = ({ image, name, rating, id, favspage }) => {
  const favCtxObj = useContext(FavContext);
  const [liked, setLiked] = useState(false);

  const authContextObj = useContext(AuthContext);
  const { netusers } = useContext(NetusersContext);
  const itemisfav = useMemo(() => favCtxObj.isfav(id), [favCtxObj, id]);

  const removefavHandler = useCallback((filmId, prevuserfavs) => {
    return prevuserfavs?.filter((el) => el.id !== filmId);
  }, []);

  const handleSaveFavs = () => {
    const togglefavorite = itemisfav
      ? removefavHandler(id, favCtxObj.favs)
      : [...(favCtxObj.favs ?? []), { id, name }];

    const idgen = netusers.find(
      (el) => el.email === authContextObj.user.email
    )?.id;
    if (idgen) {
      const body = togglefavorite ?? [];
      axios
        .put(
          `https://nenflix-default-rtdb.firebaseio.com/usersnetf/${idgen}/favs.json`,
          body
        )
        .then((response) => favCtxObj.setuserFavs(response.data));
    }
  };

  const expr = favspage && !itemisfav;

  return expr ? (
    ""
  ) : (
    <div>
      <Link to={`/singleshow/${id}`} className={classes.listitem}>
        <div className={classes.listitem__info}>
          <h4 className={classes.info__name}>{name}</h4>
          <h4 className={classes.info__rating}>{rating}</h4>
        </div>
        <img src={image} alt={name} />
      </Link>
      {authContextObj.user && (
        <>
          <FavoriteButton togglefav={handleSaveFavs} itemisfav={itemisfav} />
          <button
            className={classes.liked}
            onClick={() => setLiked((prev) => !prev)}
          >
            <i className={liked ? "fas fa-heart" : "far fa-heart"}></i>
          </button>
        </>
      )}
    </div>
  );
};

export default ListItem;
