import axios from "axios";
import React, { useCallback, useContext } from "react";
import { AuthContext } from "../firebase";
import classes from "./FriendButton.module.css";
export const FriendButton = ({
  id,
  email,
  friendsstate,
  setfriendsstate,
  current,
  favs,
}) => {
  const authContextObj = useContext(AuthContext);

  const removefavHandler = useCallback((Id, prevuserfavs) => {
    return prevuserfavs.filter((el) => el.id !== Id);
  }, []);

  const isfriendHandler = friendsstate?.some((el) => el.id === id);

  const togglefriend = () => {
    setfriendsstate((prev) =>
      isfriendHandler && prev
        ? removefavHandler(id, prev)
        : [...prev, { email, id }]
    );

    if (current.id) {
      console.log("patched", authContextObj.user);
      const body = isfriendHandler
        ? removefavHandler(id, friendsstate)
        : [...friendsstate, { email, id }];

      axios.put(
        `https://nenflix-default-rtdb.firebaseio.com/usersnetf/${current.id}/friends.json`,
        body
      );
    }
  };

  return (
    <>
      <li className={classes.user_item}>
        <p className={classes.user_email}> {email}</p>
        {authContextObj.user && (
          <>
            <button onClick={togglefriend} className={classes.add_button}>
              <i className="fas fa-user-friends"></i>
              {isfriendHandler
                ? " видалити з користувача друзів"
                : " додати до друзів"}
            </button>
            {isfriendHandler ? (
              <>
                <p> цьому користувачу подобаєтся:</p>
                {favs?.map((el2, i) => (
                  <ul>
                    <li className={classes.fav_item} key={i}>
                      <span className={classes.star}>
                        <i className="fas fa-star"></i>
                      </span>
                      {el2.name}
                    </li>
                  </ul>
                ))}
              </>
            ) : (
              <p>додайте користувача в друзі, щоб бачити його вибране</p>
            )}
          </>
        )}
      </li>
    </>
  );
};
