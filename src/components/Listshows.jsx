import React, { useContext, useMemo } from "react";
import { FilterFilmsContext } from "../context/FilterFilmsContextProvider.jsx";
import ListItem from "./ListItem.jsx";
import classes from "./Listshows.module.css";

export const Listshows = ({ shows, favspage }) => {
  const { state } = useContext(FilterFilmsContext);
  const genreMarked = state.filterByGenre !== "all" && state.filterByGenre;
  const statusMarked = state.filterByStatus !== "all" && state.filterByStatus;
  const langMarked = state.filterByLang !== "all" && state.filterByLang;

  const showsFiltered = useMemo(() => {
    return shows.filter((item) => {
      if (genreMarked && !item.genres.includes(genreMarked)) return false;
      if (langMarked && item.language !== langMarked) return false;
      if (statusMarked && item.status !== statusMarked) return false;

      return true;
    });
  }, [langMarked, genreMarked, statusMarked, shows]);

  return (
    <div className={classes.homepage__list}>
      {showsFiltered.map((item) => (
        <ListItem
          favspage={favspage}
          key={item.id}
          id={item.id}
          image={
            item.image
              ? item.image.medium
              : "https://www.publicdomainpictures.net/pictures/280000/velka/not-found-image-15383864787lu.jpg"
          }
          name={item.name}
          rating={item.rating.average ? item.rating.average : "No rating"}
        />
      ))}
    </div>
  );
};
