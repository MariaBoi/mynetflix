import classes from "./ListItem.module.css";

export const FavoriteButton = ({ itemisfav, togglefav }) => {
  return (
    <button
      className={
        itemisfav ? classes.likeb : `${classes.nolike_button} ${classes.likeb}`
      }
      onClick={togglefav}
    >
      {itemisfav ? "видалити з вибраного" : "додати до вибраного"}
    </button>
  );
};
