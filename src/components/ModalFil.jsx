import React, { useCallback, useContext, useState } from "react";
import { FilterFilmsContext } from "../context/FilterFilmsContextProvider";
import { genres, langs, status } from "../filterdata";
import { FILER_GENRE, FILER_LANG, FILER_STATUS } from "../registr/consts";
import InputFilter from "./InputFilter.jsx";
import classes from "./ModalFil.module.css";

export const ModalFil = () => {
  const { state, dispatch } = useContext(FilterFilmsContext);
  console.log(state);
  const [mod, setmod] = useState(false);
  const handleChange = useCallback(
    (namef) =>
      ({ target: { value } }) => {
        dispatch({
          type: namef,
          payload: { value },
        });
      },
    [dispatch]
  );

  return (
    <>
      <button
        className={classes.btn_modal}
        onClick={() => setmod((prev) => !prev)}
      >
        <i className="fas fa-filter">фільтрувати</i>
      </button>
      {mod && (
        <div className={classes.overlay}>
          <div className={classes.modal}>
            <button
              onClick={() => setmod((prev) => !prev)}
              className={classes.closeb}
            >
              {" "}
              закрити <i class="fa fa-times" aria-hidden="true"></i>
            </button>
            <InputFilter
              arrOfChoices={genres}
              handleChange={handleChange(FILER_GENRE)}
              label="жанрами"
              valuei={state.filterByGenre}
            />
            <InputFilter
              handleChange={handleChange(FILER_STATUS)}
              arrOfChoices={status}
              label="статусом"
              valuei={state.filterByStatus}
            />
            <InputFilter
              handleChange={handleChange(FILER_LANG)}
              arrOfChoices={langs}
              label="мовою"
              valuei={state.filterByLang}
            />
          </div>
        </div>
      )}
    </>
  );
};
