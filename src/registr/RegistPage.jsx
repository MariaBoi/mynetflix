import { useCallback } from "react";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import axios from "axios";

export const RegistPage = () => {
  const handleSubmit = useCallback(async (e) => {
    e.preventDefault();

    const { email, password } = e.target.elements;

    const auth = getAuth();

    createUserWithEmailAndPassword(auth, email.value, password.value);

    const body = {
      id: Date.now(),
      email: email.value,
    };
    axios.post(
      "https://nenflix-default-rtdb.firebaseio.com/usersnetf.json",
      body
    );
  }, []);

  return (
    <>
      <form onSubmit={handleSubmit}>
        <h1 className="cab-h">зареєструватися</h1>
        <input name="email" placeholder="email" type="email" />
        <input name="password" placeholder="password" type="password" />
        <button type="submit" className="double-bor-btn butt-lightback">
          зареєструватися
        </button>
      </form>
    </>
  );
};
