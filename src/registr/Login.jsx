import { useCallback } from "react";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";

const Login = () => {
  const handleSubmit = useCallback(async (e) => {
    e.preventDefault();

    const { email, password } = e.target.elements;
    const auth = getAuth();
    try {
      await signInWithEmailAndPassword(auth, email.value, password.value);
    } catch (e) {
      alert(e.message);
    }
  }, []);

  return (
    <>
      <form onSubmit={handleSubmit}>
        <h1 className="cab-h">Зайти зі свого аккаунту</h1>

        <input name="email" placeholder="email" type="email" />
        <input name="password" placeholder="password" type="password" />
        <button type="submit" className="double-bor-btn butt-lightback">
          зайти з аккаунту
        </button>
      </form>
    </>
  );
};

export default Login;
