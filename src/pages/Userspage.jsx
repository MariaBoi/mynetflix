import { useContext, useEffect, useState } from "react";
import { FriendButton } from "../components/FriendButton.jsx";
import { NetusersContext } from "../context/NetusersContextProvider.jsx";
import { AuthContext } from "../firebase";
import classes from "./Userspage.module.css";

const Userspage = () => {
  const { netusers } = useContext(NetusersContext);
  const authContextObj = useContext(AuthContext);
  const [friendsstate, setfriendsstate] = useState([]);

  const current = netusers?.find(
    (el) => el.email === authContextObj.user?.email
  );
  useEffect(() => {
    setfriendsstate(current?.friends ?? []);
  }, []);

  return (
    <div className={classes.container}>
      <h3 className={classes.title}>зареєстровані користувачі:</h3>

      <ul className={classes.films_list}>
        {netusers
          .filter((item) => item.id !== current?.id)
          .map((el, ind) => (
            <FriendButton
              current={current}
              key={ind}
              id={el.id}
              email={el.email}
              friendsstate={friendsstate}
              setfriendsstate={setfriendsstate}
              favs={el.favs}
            />
          ))}
      </ul>
    </div>
  );
};

export default Userspage;
