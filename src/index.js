import ReactDOM from "react-dom";
import "./index.css";
import App from "./App.jsx";
import reportWebVitals from "./reportWebVitals";
import { FavContextProvider } from "./fav-context.jsx";
import LoaderContextProvider from "./context/LoaderContextProvider.jsx";
import { AuthContextProvider } from "./firebase.jsx";
import { FilterFilmsContextProvider } from "./context/FilterFilmsContextProvider.jsx";
import NetusersContextProvider from "./context/NetusersContextProvider.jsx";

ReactDOM.render(
  <FilterFilmsContextProvider>
    <LoaderContextProvider>
      <AuthContextProvider>
        <FavContextProvider>
          <NetusersContextProvider>
            <App />
          </NetusersContextProvider>
        </FavContextProvider>
      </AuthContextProvider>
    </LoaderContextProvider>
  </FilterFilmsContextProvider>,
  document.getElementById("root")
);

reportWebVitals();
