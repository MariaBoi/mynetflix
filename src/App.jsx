import Axios from "axios";
import { useContext, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Navbar from "./components/Navbar";
import { LoaderContext } from "./context/LoaderContextProvider.jsx";
import { NetusersContext } from "./context/NetusersContextProvider.jsx";
import FavContext from "./fav-context.jsx";
import { AuthContext } from "./firebase.jsx";

import AppRouter from "./registr/AppRouter";

const App = () => {
  const { setIsLoading } = useContext(LoaderContext);
  const { netusers, setnetusers } = useContext(NetusersContext);
  const { setuserFavs } = useContext(FavContext);
  const authContextObj = useContext(AuthContext);

  useEffect(() => {
    setIsLoading(true);
    Axios.get(
      "https://nenflix-default-rtdb.firebaseio.com/usersnetf.json"
    ).then((respon) => {
      const newmets = [];
      const data = respon.data;
      for (let keyy in data) {
        console.log(data[keyy]);
        const metobj = { ...data[keyy], id: keyy };
        newmets.push(metobj);
      }
      setnetusers(newmets);
      setIsLoading(false);
    });
  }, [authContextObj.user?.email]);

  useEffect(() => {
    if (authContextObj.user) {
      const izbr = netusers.filter((el) => {
        return el.email === authContextObj.user.email;
      })[0]?.favs;
      izbr ? setuserFavs(izbr) : setuserFavs([]);
    }
  }, [authContextObj.user?.email]);

  return (
    <Router>
      <Navbar />
      <div className="container">
        <AppRouter />
      </div>
    </Router>
  );
};

export default App;
